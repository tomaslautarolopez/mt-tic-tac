# MrTicTac

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `mr_tic_tac` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:mr_tic_tac, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/mr_tic_tac](https://hexdocs.pm/mr_tic_tac).

## TO DO

- Replace hardcoded statistics value with something meaningful

## Start the server

Start the server with the following command:

```bash
$ iex -S mix
```

### How we represent the game state

Host (X) goes first. Guest (O) goes after. We'll represent the game state via a list of plays, 
where each play is an ordered pair where the first element is the row and the second element is
the column. For example:

```
[(0, 0), (2, 2)]
```

corresponds to the following MrTicTac table:

```
X| |
-----
 | |
-----
 | |O
```