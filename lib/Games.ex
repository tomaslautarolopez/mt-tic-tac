defmodule MrTicTac.Games do
  use GenServer

  @name :games_server

  def start_link() do
    IO.puts("Starting the games state")
    GenServer.start_link(__MODULE__, %{}, name: @name)
  end

  def init(initial_state) do
    {:ok, initial_state}
  end

  def handle_cast({:new_game, user_name}, state) do
    {:noreply,
     Map.put(state, user_name, %{
       guest: :"no-player",
       plays: [],
       observers: []
     })}
  end

  def handle_call(:list_games, _from, state) do
    {:reply, state, state}
  end

  def handle_call({:join_game, user_name, game_id}, _from, state) do
    case Map.get(state, game_id) do
      nil ->
        {:reply, :not_valid_game, state}

      game ->
        case Map.get(game, :guest) do
          :"no-player" ->
            Enum.each(game.observers, fn observer ->
              MrTicTac.Utils.send_to_user(observer, "Game has started \n")
            end)

            MrTicTac.Utils.send_to_user(game_id, "Game has started \n")
            MrTicTac.Utils.send_to_user(game_id, "Your turn \n")

            MrTicTac.Utils.send_to_user(user_name, "Game has started \n")

            {:reply, :joined, Map.put(state, game_id, Map.put(game, :guest, user_name))}

          _ ->
            {:reply, :game_full, state}
        end
    end
  end

  def handle_call({:is_user_playing, user_name}, _from, state) do
    is_host = Map.get(state, user_name)

    is_guest =
      Map.values(state)
      |> Enum.reduce(false, fn game, acc ->
        guest = game.guest

        if guest === user_name do
          true
        else
          acc
        end
      end)

    {:reply, is_host || is_guest, state}
  end

  def handle_call({:is_user_playing_in_game, user_name, game_id}, _from, state) do
    is_host = Map.get(state, user_name) && user_name === game_id

    game = Map.get(state, game_id)
    is_guest = game && game.guest === user_name

    {:reply, is_host || is_guest, state}
  end

  def handle_call({:observe_game, user_name, game_id}, _from, state) do
    case Map.get(state, game_id) do
      nil ->
        {:reply, :not_valid_game, state}

      game ->
        observers = Map.get(game, :observers)

        if Enum.member?(observers, user_name) do
          {:reply, :observing, state}
        else
          {:reply, :observing,
           Map.put(state, game_id, Map.put(game, :observers, [user_name | observers]))}
        end
    end
  end

  def handle_call({:make_play, user_name, game_id, row, column}, _from, state) do
    case Map.get(state, game_id) do
      nil ->
        IO.puts("Game not found bro\n")
        {:reply, :not_valid_game, state}

      game ->
        row = elem(Integer.parse(row), 0)
        column = elem(Integer.parse(column), 0)
        play = {row, column}
        is_guest = game.guest == user_name
        play_already_exists = Enum.member?(game.plays, play)
        valid_row = row >= 0 && row <= 2
        valid_column = column >= 0 && column <= 2
        observers = game.observers
        guest = game.guest
        host = game_id

        valid_turn =
          (is_guest && rem(length(game.plays), 2) == 1) ||
            (not is_guest && rem(length(game.plays), 2) == 0)

        valid_play = valid_row && valid_column && not play_already_exists && valid_turn

        if valid_play do
          new_plays_state = game.plays ++ [play]
          has_win = check_winner(new_plays_state)

          if is_guest do
            MrTicTac.Utils.send_to_user(host, "Your turn for game #{game_id}\n")
          else
            MrTicTac.Utils.send_to_user(guest, "Your turn for game #{game_id}\n")
          end

          table = MrTicTac.PrettyPrint.table(new_plays_state)

          Enum.each(observers, fn observer ->
            MrTicTac.Utils.send_to_user(observer, table)
          end)

          MrTicTac.Utils.send_to_user(host, table)
          MrTicTac.Utils.send_to_user(guest, table)

          if has_win !== :none do
            Enum.each(observers, fn observer ->
              MrTicTac.Utils.send_to_user(observer, "#{has_win} has win!\n")
            end)

            MrTicTac.Utils.send_to_user(host, "#{has_win} has win!\n")
            MrTicTac.Utils.send_to_user(guest, "#{has_win} has win!\n")

            {:reply, {:ok, new_plays_state}, Map.delete(state, game_id)}
          else
            {:reply, {:ok, new_plays_state},
             Map.put(state, game_id, Map.put(game, :plays, new_plays_state))}
          end
        else
          {:reply, :error, state}
        end
    end
  end

  def handle_call({:remove_user, user_name}, _from, state) do
    case Map.get(state, user_name) do
      nil ->
        {:reply, :not_valid_game, state}

      game ->
        observers = Map.get(game, :observers)

        Enum.each(observers, fn observer ->
          MrTicTac.Utils.send_to_user(observer, "#{user_name}")
        end)
        MrTicTac.Utils.send_to_user(guest, table)

        if Enum.member?(observers, user_name) do
          {:reply, :observing, state}
        else
          {:reply, :observing,
           Map.put(state, game_id, Map.put(game, :observers, [user_name | observers]))}
        end
    end
  end

  def check_combination_win(p1, p2, p3) do
    Enum.all?([p1, p2, p3], fn x -> x === p1 end) and p1 !== " "
  end

  def check_winner(plays) do
    {[[p11, p12, p13], [p21, p22, p23], [p31, p32, p33]], _} =
      MrTicTac.Utils.plays_to_table(plays)

    cond do
      check_combination_win(p11, p12, p13) ->
        p11

      check_combination_win(p21, p22, p23) ->
        p21

      check_combination_win(p31, p32, p33) ->
        p31

      check_combination_win(p11, p21, p31) ->
        p11

      check_combination_win(p12, p22, p32) ->
        p12

      check_combination_win(p13, p23, p33) ->
        p13

      check_combination_win(p11, p22, p33) ->
        p11

      check_combination_win(p13, p22, p31) ->
        p13

      true ->
        :none
    end
  end

  def list_games(node, caller_pid) do
    games = GenServer.call(@name, :list_games)

    Node.spawn(node, fn -> send(caller_pid, {:list_games, games}) end)
  end

  def is_user_playing(node, caller_pid, user_name) do
    is_playing = GenServer.call(@name, {:is_user_playing, user_name})

    Node.spawn(node, fn -> send(caller_pid, {:is_user_playing, is_playing}) end)
  end

  def is_user_playing_in_game(node, caller_pid, user_name, game_id) do
    is_playing = GenServer.call(@name, {:is_user_playing_in_game, user_name, game_id})

    Node.spawn(node, fn -> send(caller_pid, {:is_user_playing_in_game, is_playing}) end)
  end

  def new_game(user_name) do
    GenServer.cast(@name, {:new_game, user_name})
  end

  @spec join_game(atom, any, any, any) :: pid
  def join_game(node, caller_pid, game_id, user_name) do
    response = GenServer.call(@name, {:join_game, user_name, game_id})

    Node.spawn(node, fn -> send(caller_pid, response) end)
  end

  @spec observe_game(atom, any, any, any) :: pid
  def observe_game(node, caller_pid, game_id, user_name) do
    response = GenServer.call(@name, {:observe_game, user_name, game_id})

    Node.spawn(node, fn -> send(caller_pid, response) end)
  end

  def make_play(node, caller_pid, user_name, game_id, row, column) do
    response = GenServer.call(@name, {:make_play, user_name, game_id, row, column})

    Node.spawn(node, fn -> send(caller_pid, response) end)
  end

  def remove_user(user_name) do
    GenServer.call(@name, {:remove_user, user_name})
  end
end
