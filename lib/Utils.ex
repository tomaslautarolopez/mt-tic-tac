defmodule MrTicTac.Utils do
  def send_to_all_nodes(fn_to_exec) do
    Enum.map([Node.self() | Node.list()], fn node ->
      Task.async(fn -> fn_to_exec.(node) end)
    end)
    |> Task.await_many(5000)
  end

  def send_to_user(user_name, message) do
    MrTicTac.Utils.send_to_all_nodes(fn node ->
      Node.spawn(node, fn ->
        MrTicTac.Connections.send_to_user(user_name, message)
      end)
    end)
  end

  def send_to_node(node, pid, message) do
    Node.spawn(node, fn ->
      send(pid, message)
    end)
  end

  def user_exists_in_node(node, user_name) do
    current_node = Node.self()
    current_pid = self()

    Node.spawn(node, fn ->
      MrTicTac.Connections.user_exists(current_node, current_pid, user_name)
    end)

    receive do
      {:user_exists, exists} -> exists
    end
  end

  def plays_to_table(plays) do
    empty_table = {[[" ", " ", " "], [" ", " ", " "], [" ", " ", " "]], :host}

    Enum.reduce(plays, empty_table, fn {x, y}, {table, token} ->
      new_table = List.replace_at(table, x, List.replace_at(Enum.at(table, x), y, token))

      if(token === :host) do
        {new_table, :guest}
      else
        {new_table, :host}
      end
    end)
  end
end
