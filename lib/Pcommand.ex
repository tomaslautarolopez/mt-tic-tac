defmodule MrTicTac.Pcommand do
  def is_user_playing(user_name) do
    MrTicTac.Utils.send_to_all_nodes(fn node ->
      current_node = Node.self()
      current_pid = self()

      Node.spawn(node, fn ->
        MrTicTac.Games.is_user_playing(current_node, current_pid, user_name)
      end)

      receive do
        {:is_user_playing, playing} -> playing
      end
    end)
    |> Enum.reduce(fn exists, acc -> exists || acc end)
  end

  def games_in_node(node) do
    current_node = Node.self()
    current_pid = self()

    Node.spawn(node, fn ->
      MrTicTac.Games.list_games(current_node, current_pid)
    end)

    receive do
      {:list_games, games} -> games
    end
  end

  def join_game(game_id, user_name) do
    MrTicTac.Utils.send_to_all_nodes(fn node ->
      current_node = Node.self()
      current_pid = self()

      Node.spawn(node, fn ->
        MrTicTac.Games.join_game(current_node, current_pid, game_id, user_name)
      end)

      receive do
        playing -> playing
      end
    end)
    |> Enum.reduce(:not_valid_game, fn game_result, acc ->
      case game_result do
        :joined -> :joined
        :game_full -> :game_full
        _ -> acc
      end
    end)
  end

  def observe_game(game_id, user_name) do
    MrTicTac.Utils.send_to_all_nodes(fn node ->
      current_node = Node.self()
      current_pid = self()

      Node.spawn(node, fn ->
        MrTicTac.Games.observe_game(current_node, current_pid, game_id, user_name)
      end)

      receive do
        observing -> observing
      end
    end)
    |> Enum.reduce(:not_valid_game, fn game_result, acc ->
      case game_result do
        :observing -> :observing
        _ -> acc
      end
    end)
  end

  def make_play(game_id, user_name, row, column) do
    MrTicTac.Utils.send_to_all_nodes(fn node ->
      current_node = Node.self()
      current_pid = self()

      Node.spawn(node, fn ->
        MrTicTac.Games.make_play(current_node, current_pid, game_id, user_name, row, column)
      end)

      receive do
        response -> response
      end
    end)
    |> Enum.reduce(:error, fn play_result, acc ->
      case play_result do
        {:ok, new_game_state} -> {:ok, new_game_state}
        _ -> acc
      end
    end)
  end

  def pcommand("CON " <> user_name, args) do
    %{node: node, pid: pid, socket_user_name: socket_user_name} = args

    if socket_user_name !== :"not-registered" do
      MrTicTac.Utils.send_to_node(node, pid, {:error, "You are already registered\n"})
    else
      user_exists =
        MrTicTac.Utils.send_to_all_nodes(fn node ->
          MrTicTac.Utils.user_exists_in_node(node, user_name)
        end)
        |> Enum.reduce(fn exists, acc -> exists || acc end)

      IO.puts("This command was handled by #{Node.self()} and was invoked by #{node}")

      if user_exists do
        MrTicTac.Utils.send_to_node(node, pid, {:error, "User already exists\n"})
      else
        MrTicTac.Utils.send_to_node(
          node,
          pid,
          {:"user-connected", "Connection succesful - Welcome #{user_name}!\n", user_name}
        )
      end
    end
  end

  def pcommand("LSG" <> _, args) do
    %{node: node, pid: pid} = args

    MrTicTac.Middlewares.user_should_be_logged(
      args,
      fn ->
        games =
          MrTicTac.Utils.send_to_all_nodes(fn node ->
            games_in_node(node)
          end)
          |> Enum.reduce(fn exists, acc -> Map.merge(exists, acc) end)
          |> MrTicTac.PrettyPrint.games()

        IO.puts("This command was handled by #{Node.self()} and was invoked by #{node}")

        Node.spawn(node, fn ->
          send(
            pid,
            {:"list-games", "Games: \n#{games}"}
          )
        end)
      end
    )
  end

  def pcommand("NEW" <> _, args) do
    %{node: node, pid: pid, socket_user_name: socket_user_name} = args

    MrTicTac.Middlewares.user_should_be_logged(
      args,
      fn ->
        is_playing = is_user_playing(socket_user_name)

        IO.puts("This command was handled by #{Node.self()} and was invoked by #{node}")

        if is_playing do
          MrTicTac.Utils.send_to_node(node, pid, {:error, "User already playing\n"})
        else
          MrTicTac.Utils.send_to_node(node, pid, {:"new-game", "You create a new game\n"})
        end
      end
    )
  end

  def pcommand("ACC " <> game_id, args) do
    %{node: node, pid: pid, socket_user_name: socket_user_name} = args

    MrTicTac.Middlewares.user_should_be_logged(
      args,
      fn ->
        is_playing = is_user_playing(socket_user_name)

        IO.puts("This command was handled by #{Node.self()} and was invoked by #{node}")

        if is_playing do
          MrTicTac.Utils.send_to_node(node, pid, {:error, "User already playing\n"})
        else
          case join_game(game_id, socket_user_name) do
            :not_valid_game ->
              MrTicTac.Utils.send_to_node(node, pid, {:error, "That game does not exist\n"})

            :joined ->
              MrTicTac.Utils.send_to_node(
                node,
                pid,
                {:"join-game", "You joined #{game_id} game\n"}
              )

            :game_full ->
              MrTicTac.Utils.send_to_node(node, pid, {:"join-game", "#{game_id} is full\n"})
          end
        end
      end
    )
  end

  def pcommand("OBS " <> game_id, args) do
    %{node: node, pid: pid, socket_user_name: socket_user_name} = args

    MrTicTac.Middlewares.user_should_be_logged(
      args,
      fn ->
        IO.puts("This command was handled by #{Node.self()} and was invoked by #{node}")

        case observe_game(game_id, socket_user_name) do
          :not_valid_game ->
            MrTicTac.Utils.send_to_node(node, pid, {:error, "That game does not exist\n"})

          :observing ->
            MrTicTac.Utils.send_to_node(
              node,
              pid,
              {:"observe-game", "You joined #{game_id}'s game as an observer\n"}
            )
        end
      end
    )
  end

  def pcommand("BYE" <> _, args) do
    %{node: node, pid: pid, socket_user_name: socket_user_name} = args

    MrTicTac.Middlewares.user_should_be_logged(
      args,
      fn ->
        IO.puts("This command was handled by #{Node.self()} and was invoked by #{node}")

        user_deleted =
          MrTicTac.Utils.send_to_all_nodes(fn node ->
            current_node = Node.self()
            current_pid = self()

            Node.spawn(node, fn ->
              MrTicTac.Connections.remove_user(current_node, current_pid, socket_user_name)
            end)

            receive do
              playing -> playing
            end
          end)
          |> Enum.reduce(fn response, acc ->
            case response do
              :ok -> :ok
              :not_found -> acc
            end
          end)

        case user_deleted do
          :ok ->
            MrTicTac.Utils.send_to_node(
              node,
              pid,
              {:"user-removed", "User disconected succesfully\n"}
            )

          :not_found ->
            MrTicTac.Utils.send_to_node(node, pid, {:error, "We could not disconect you\n"})
        end
      end
    )
  end

  def pcommand("PLA " <> play, args) do
    %{node: node, pid: pid, socket_user_name: socket_user_name} = args
    [game_id, row, column] = String.split(play)

    MrTicTac.Middlewares.user_should_be_logged(
      args,
      fn ->
        MrTicTac.Middlewares.is_user_playing_give_game(Map.put(args, :game_id, game_id), fn ->
          IO.puts(
            "This command was handled by #{Node.self()} and was invoked by #{node} - is_playing_in_game: #{
              game_id
            }"
          )

          case make_play(socket_user_name, game_id, row, column) do
            {:ok, _} ->
              MrTicTac.Utils.send_to_node(
                node,
                pid,
                {:ok, "Play successfully handled\n"}
              )

            :error ->
              MrTicTac.Utils.send_to_node(node, pid, {:error, "Error handling play\n"})
          end
        end)
      end
    )
  end

  def pcommand(_, args) do
    %{node: node, pid: pid} = args

    MrTicTac.Utils.send_to_node(node, pid, {:error, "This command is bull SHIT!\n"})
  end
end
