defmodule MrTicTac.PrettyPrint do
  def games(games) do
    games_keys = Map.keys(games)

    Enum.reduce(games_keys, "", fn game_host, acc ->
      game = Map.get(games, game_host)

      if game.guest === :"no-player" do
        "#{acc}Game by: #{game_host} - no players \n observers: #{inspect(game.observers)}\n"
      else
        "#{acc}Game by: #{game_host} against #{game.guest} \n observers: #{
          inspect(game.observers)
        }\n"
      end
    end)
  end

  def atom_to_token(:host) do
    "X"
  end

  def atom_to_token(:guest) do
    "O"
  end

  def atom_to_token(x) do
    x
  end


  def table(plays) do
    {[[p11, p12, p13], [p21, p22, p23], [p31, p32, p33]], _} =
      MrTicTac.Utils.plays_to_table(plays)

    "#{atom_to_token(p11)}|#{atom_to_token(p12)}|#{atom_to_token(p13)}\n#{atom_to_token(p21)}|#{
      atom_to_token(p22)
    }|#{atom_to_token(p23)}\n#{atom_to_token(p31)}|#{atom_to_token(p32)}|#{atom_to_token(p33)}\n"
  end
end
