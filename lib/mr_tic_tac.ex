defmodule MrTicTac do
  @moduledoc """
  Documentation for `MrTicTac`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> MrTicTac.hello()
      :world

  """
  def hello do
    :world
  end
end
