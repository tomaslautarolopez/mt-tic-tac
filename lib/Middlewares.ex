defmodule MrTicTac.Middlewares do
  def user_should_be_logged(args, next) do
    %{node: node, pid: pid, socket_user_name: socket_user_name} = args

    if socket_user_name === :"not-registered" do
      MrTicTac.Utils.send_to_node(node, pid, {:error, "You need to register first\n"})
    else
      next.()
    end
  end

  def is_user_playing_give_game(args, next) do
    %{socket_user_name: socket_user_name, game_id: game_id, pid: pid, node: node} = args

    playing_in_game =
      MrTicTac.Utils.send_to_all_nodes(fn node ->
        current_node = Node.self()
        current_pid = self()

        Node.spawn(node, fn ->
          MrTicTac.Games.is_user_playing_in_game(current_node, current_pid, socket_user_name, game_id)
        end)

        receive do
          {:is_user_playing_in_game, playing} -> playing
        end
      end)
      |> Enum.reduce(fn exists, acc -> exists || acc end)

    if playing_in_game do
      next.()
    else
      MrTicTac.Utils.send_to_node(node, pid, {:error, "You're not in the given game!\n"})
    end
  end
end
