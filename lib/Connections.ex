defmodule MrTicTac.Connections do
  use GenServer

  @name :connections_server

  def start_link() do
    IO.puts("Starting the connections state")
    GenServer.start_link(__MODULE__, %{}, name: @name)
  end

  def init(initial_state) do
    {:ok, initial_state}
  end

  def handle_cast({:new_user, name, socket}, state) do
    {:noreply, Map.put(state, name, socket)}
  end

  def handle_call(:get_users, _from, state) do
    {:reply, state, state}
  end

  def handle_call({:get_user, name}, _from, state) do
    {:reply, state[name], state}
  end

  def handle_call({:user_exists, name}, _from, state) do
    case state[name] do
      nil ->
        {:reply, false, state}

      _ ->
        {:reply, true, state}
    end
  end

  def handle_call({:remove_user, name}, _from, state) do
    case state[name] do
      nil ->
        {:reply, :not_found, state}

      _ ->
        {:reply, :ok, Map.delete(state, name)}
    end
  end

  def get_users() do
    GenServer.call(@name, :get_users)
  end

  def get_user(name) do
    GenServer.call(@name, {:get_user, name})
  end

  def user_exists(node, caller_pid, name) do
    exists = GenServer.call(@name, {:user_exists, name})

    Node.spawn(node, fn -> send(caller_pid, {:user_exists, exists}) end)
  end

  def new_user(name, socket) do
    GenServer.cast(@name, {:new_user, name, socket})
  end

  def remove_user(node, caller_pid, name) do
    response = GenServer.call(@name, {:remove_user, name})

    Node.spawn(node, fn -> send(caller_pid, response) end)
  end

  def send_to_user(user, message) do
    socket = MrTicTac.Connections.get_user(user)

    :gen_tcp.send(socket, message)
  end
end
