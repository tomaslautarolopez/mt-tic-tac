defmodule MrTicTac.NodesLoads do
  use GenServer

  @name :node_loads

  def start_link() do
    IO.puts("Starting the statistics state")
    GenServer.start_link(__MODULE__, %{}, name: @name)
  end

  def init(initial_state) do
    {:ok, initial_state}
  end

  def handle_cast({:new_node_load, node, load}, state) do
    {:noreply, Map.put(state, node, load)}
  end

  def handle_call(:get_loads, _from, state) do
    {:reply, state, state}
  end

  def handle_call({:get_node_load, node}, _from, state) do
    {:reply, state[node], state}
  end

  def handle_call({:get_lazy_node}, _from, state) do
    {:reply, minimum_value_of_map(Map.keys(state), state), state}
  end

  def minimum_value_of_map([], _map, min_key) do
    min_key
  end

  def minimum_value_of_map([key | rest], map, min_key) do
    new_min_value = Map.get(map, key)
    current_min_value = Map.get(map, min_key)

    if new_min_value < current_min_value do
      minimum_value_of_map(rest, map, key)
    else
      minimum_value_of_map(rest, map, min_key)
    end
  end

  def minimum_value_of_map([key | rest], map) do
    minimum_value_of_map(rest, map, key)
  end

  def minimum_value_of_map([], _map) do
    nil
  end

  def get_loads() do
    GenServer.call(@name, :get_loads)
  end

  def get_lazy_node() do
    GenServer.call(@name, {:get_lazy_node})
  end

  def get_node_load(node) do
    GenServer.call(@name, {:get_node_load, node})
  end

  def new_node_load(node, load) do
    GenServer.cast(@name, {:new_node_load, node, load})
  end
end
