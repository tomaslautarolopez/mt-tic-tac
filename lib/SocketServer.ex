defmodule MrTicTac.SocketServer do
  @doc """
  Starts the server on the given `port` of localhost.
  """
  def start(port) when is_integer(port) and port > 1023 do
    MrTicTac.NodesLoads.start_link()
    MrTicTac.Connections.start_link()
    MrTicTac.Games.start_link()

    spawn(fn -> statistics() end)
    pid = spawn(fn -> load_balancer() end)

    Process.register(pid, :pbalance)
    # Creates a socket to listen for client connections.
    # `listen_socket` is bound to the listening socket.
    {:ok, listen_socket} =
      :gen_tcp.listen(port, [:binary, packet: :raw, active: false, reuseaddr: true])

    # Socket options (don't worry about these details):
    # `:binary` - open the socket in "binary" mode and deliver data as binaries
    # `packet: :raw` - deliver the entire binary without doing any packet handling
    # `active: false` - receive data when we're ready by calling `:gen_tcp.recv/2`
    # `reuseaddr: true` - allows reusing the address if the listener crashes

    IO.puts("\n🎧  Listening for connection requests on port #{port}...\n")

    accept_loop(listen_socket)
  end

  @spec start(1..1_114_111, atom) :: no_return
  def start(port, node) when is_integer(port) and port > 1023 and is_atom(node) do
    # Creates a socket to listen for client connections.
    # `listen_socket` is bound to the listening socket.
    Node.connect(node)

    start(port)
  end

  def statistics do
    my_node = Node.self()

    stat = :rand.uniform(50)

    MrTicTac.NodesLoads.new_node_load(Node.self(), stat)

    Enum.map(Node.list(), fn node ->
      Node.spawn(node, fn -> send(:pbalance, {:node_stat, my_node, stat}) end)
    end)

    :timer.sleep(5000)

    statistics()
  end

  def load_balancer() do
    receive do
      {:node_stat, node, load} ->
        IO.puts("\nNew node load #{node}: #{load}\n")
        MrTicTac.NodesLoads.new_node_load(node, load)
        IO.puts("\nNode loads #{inspect(MrTicTac.NodesLoads.get_loads())}\n")
    end

    load_balancer()
  end

  @doc """
  Accepts client connections on the `listen_socket`.
  """
  def accept_loop(listen_socket) do
    IO.puts("⌛️  Waiting to accept a client connection...\n")

    # Suspends (blocks) and waits for a client connection. When a connection
    # is accepted, `client_socket` is bound to a new client socket.
    {:ok, client_socket} = :gen_tcp.accept(listen_socket)

    IO.puts(inspect(client_socket))

    IO.puts("⚡️  Connection accepted!\n")

    # Receives the request and sends a response over the client socket.
    spawn(fn -> serve({client_socket, :"not-registered"}) end)

    IO.puts("➡️  Ner User Provided:\n")

    # Loop back to wait and accept the next connection.
    accept_loop(listen_socket)
  end

  @doc """
  Receives the request on the `client_socket` and
  sends a response back over the same socket
  """
  def serve({client_socket, user_name}) do
    IO.puts("#{inspect(self())}: Working on it!")

    {client_socket, user_name}
    |> read_request
    |> write_response
    |> serve
  end

  @doc """
  Receives a request on the `client_socket`.
  """
  def read_request({client_socket, user_name}) do
    # all available bytes
    case :gen_tcp.recv(client_socket, 0) do
      {:ok, request} ->
        IO.puts("➡️  Received request:\n")
        IO.puts(request)

        lazy_node = MrTicTac.NodesLoads.get_lazy_node()
        current_node = Node.self()
        curren_pid = self()

        Node.spawn(lazy_node, fn ->
          MrTicTac.Pcommand.pcommand(String.trim(request), %{
            node: current_node,
            pid: curren_pid,
            socket_user_name: user_name
          })
        end)

        receive do
          {:"user-connected", response, new_user_name} ->
            MrTicTac.Connections.new_user(new_user_name, client_socket)
            {response, client_socket, new_user_name}

          {:"list-games", response} ->
            {response, client_socket, user_name}

          {:"new-game", response} ->
            MrTicTac.Games.new_game(user_name)
            {response, client_socket, user_name}

          {:"join-game", response} ->
            {response, client_socket, user_name}

          {:"user-removed", response} ->
            :ok = :gen_tcp.send(client_socket, response)
            :gen_tcp.close(client_socket)

            Process.exit(self(), :"user-disconnected")

          {_, response} ->
            {response, client_socket, user_name}
        end

      {:error, _} ->
        if(user_name !== :"not-registered") do
          MrTicTac.Connections.remove_user(Node.self(), self(), user_name)
        end

        Process.exit(self(), :"user-disconnected")
    end
  end

  @doc """
  Sends the `response` over the `client_socket`.
  """
  def write_response({response, client_socket, user_name}) do
    :ok = :gen_tcp.send(client_socket, response)

    # Closes the client socket, ending the connection.
    # Does not close the listen socket!
    {client_socket, user_name}
  end
end
